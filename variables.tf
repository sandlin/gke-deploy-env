variable "project" {
  description = "The GCP Project"
  type = string
}

variable "region" {
  description = "The GCP Region"
  type = string
}

variable "zone" {
  description = "The GCP Zone"
  type = string
}

variable "subnet_cidr" {
  description = "Subnet IP Range"
  type = string
}
variable "cluster_name" {
    description = "The name of your Kubernetes Cluster"
    type = string
}

variable "gke_num_nodes" {
    description = "Number of GKE Nodes in the cluster"
    default = 2
    type = number
}

variable "cluster_sa_id" {
    description = "The ID for the service account which will manage your cluster."
    default = "gitlab-cluster-sa"
    type = string
}

variable "cluster_sa_name" {
    description = "The name / description for the cluster management service account."
    default = "GitLab K8S Cluster Admin"
    type = string
}

variable "gitlab_username" {
  type = string
  default     = ""
}
variable "gitlab_token" {
  type = string
  default     = ""
}

variable "gitlab_project_id_agent_config" {
  description = "The id of the GitLab project which stores your agent configs"
  type = number
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

variable "service_account_file" {
  description = "The path to your creds file. Will be used by GitLab project vars to pass creds (until we have vault in place)"
  default     = ""
}
# variable "service_account_file" {
#   description = "The service account id to use; this value will only be used if service_account_json is not set."
#   default     = ""
# }
